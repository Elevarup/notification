package main

import (
	"gitlab.com/Elevarup/notification/notifier"
	"gitlab.com/Elevarup/notification/observer"
)

func main() {
	notificador := notifier.Notifier{}
	telegramObserver := &observer.Telegram{
		Url:      "https://api.telegram.org/bot:token/sendMessage?chat_id=:chatId&&text=:text",
		ChatId:   "@elevar005",
		TokenBot: "5351997198:AAGr2cRz2nw6WJ_3z5pdyCaqlh8j89DMMcY",
	}

	notificador.Add("t-1", telegramObserver)
	notificador.Msg = "mensaje desde la terminal de linux"

	notificador.NotifyObservers()
}
