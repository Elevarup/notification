package observer

import (
	"log"
	"net/http"
	"strings"
)

type (
	Telegram struct {
		Url      string
		TokenBot string
		ChatId   string
	}
)

func (t *Telegram) Notify(message string) {
	url := t.Url

	url = strings.ReplaceAll(url, ":token", t.TokenBot)
	url = strings.ReplaceAll(url, ":chatId", t.ChatId)
	url = strings.ReplaceAll(url, ":text", message)

	client := http.Client{}

	_, err := client.Get(url)
	if err != nil {
		log.Println("Error send notification telegram, err: ", err)
	}

}
