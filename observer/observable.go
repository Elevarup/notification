package observer

type (
	Observable interface {
		Add(name string, o Observer)
		Remove(name string)
		NotifyObservers()
	}
)
