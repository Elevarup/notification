package observer

type (
	Observer interface {
		Notify(message string)
	}
)
