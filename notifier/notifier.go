package notifier

import (
	"sync"

	"gitlab.com/Elevarup/notification/observer"
)

type (
	Notifier struct {
		Msg       string
		observers map[string]observer.Observer
	}
)

func (n *Notifier) Add(name string, o observer.Observer) {
	if n.observers == nil {
		n.observers = make(map[string]observer.Observer)
	}

	n.observers[name] = o
}
func (n *Notifier) Remove(name string) {
	delete(n.observers, name)
}
func (n *Notifier) NotifyObservers() {

	var wg sync.WaitGroup

	for _, ob := range n.observers {
		wg.Add(1)

		go func(o observer.Observer, wGroup *sync.WaitGroup, msg string) {
			defer wGroup.Done()

			o.Notify(msg)

		}(ob, &wg, n.Msg)
	}

	wg.Wait()
}
